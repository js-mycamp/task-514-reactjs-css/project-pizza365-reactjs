const { Component } = require("react");

class header extends Component {
    render() {
        return(
            <div class="container-fluid bg-light">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse bg-warning" id="navbarNav">
                                <ul class="navbar-nav nav-fill w-100">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Trang Chủ</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#combo">Combo</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#type">Loại Pizza</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#contact">Gửi Đơn hàng</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}
export default header