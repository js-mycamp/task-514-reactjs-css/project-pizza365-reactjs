import { Component } from "react";
import hawaiian from "../../../assets/Images/hawaiian.jpg"
import seafood from "../../../assets/Images/seafood.jpg"
import bacon from "../../../assets/Images/bacon.jpg"
class type extends Component {
    render(){
        return(
            <div>
                 <div id="type" className="row">
                 
                    <div className="col-sm-12 text-center p-4 mt-4">
                        <h2><b className="p-2 border-bottom text-warning">Chọn loại pizza</b></h2>
                    </div>

                   
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-4">
                                <div className="card w-100" style={{width:"18rem"}}>
                                    <img src={seafood} className="card-img-top" />
                                    <div className="card-body">
                                        <h4>OCENA MANIA</h4>
                                        <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                        <p>
                                            Xốt Cà Chua, Phô Mai Mozzarella, Tôm Mực, Thanh Cua, Hành Tây.
                                        </p>
                                        <p>
                                            <button className="btn btn-warning w-100" id="btn-pizza-hai-san"
                                                data-is-selected-pizza="N">
                                                Chọn
                                            </button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card w-100" style={{width:"18rem"}}>
                                    <img src={hawaiian} className="card-img-top" />
                                    <div className="card-body">
                                        <h4>HAWAIIAN</h4>
                                        <p>PIZZA DĂM BÔNG DỪA KIỂU HAWAII</p>
                                        <p>
                                            Xốt Cà Chua,Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.
                                        </p>
                                        <p>
                                            <button className="btn btn-warning w-100" id="btn-pizza-hawaii"
                                                data-is-selected-pizza="N">
                                                Chọn
                                            </button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card w-100" style={{width:"18rem"}}>
                                    <img src={bacon} className="card-img-top" />
                                    <div className="card-body">
                                        <h4>CHEESY CHICKEN BACON</h4>
                                        <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                        <p>
                                            Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua
                                        </p>
                                        <p>
                                            <button className="btn btn-warning w-100" id="btn-pizza-bacon"
                                                data-is-selected-pizza="N">
                                                Chọn
                                            </button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default type