const { Component } = require("react");

class form extends Component{
    render(){
        return(
            <div>
                   <div id="contact" className="row">
                
                    <div className="col-sm-12 text-center p-4 mt-4">
                        <h2><b className="p-2 border-bottom text-warning">Thông tin đơn hàng</b></h2>
                    </div>

                
                    <div className="col-sm-12 p-2 jumbotron">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <label for="inp-name">Họ và tên</label>
                                    <input type="text" className="form-control" id="inp-name" placeholder="Họ và tên" />
                                </div>
                                <div className="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" className="form-control" id="inp-email" placeholder="Email" />
                                </div>
                                <div className="form-group">
                                    <label for="inp-phone">Điện thoại</label>
                                    <input type="text" className="form-control" id="inp-phone" placeholder="Điện thoại" />
                                </div>
                                <div className="form-group">
                                    <label for="inp-address">Địa chỉ</label>
                                    <input type="text" className="form-control" id="inp-address" placeholder="Địa chỉ" />
                                </div>
                                <div className="form-group">
                                    <label for="message">Lời nhắn</label>
                                    <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn" />
                                </div>
                                <div className="form-group">
                                    <label for="inp-voucher">Mã giảm giá ( Voucher ID)</label>
                                    <input type="text" className="form-control" id="inp-voucher"
                                        placeholder="Mã voucher..." />
                                </div>
                                <button type="button" className="btn btn-warning w-100" id="btn-makeOder">
                                    Gửi
                                </button>

                            </div>
                        </div>
                    </div>
                    {/* <!-- vùng hiển thị thông tin đơn hàng(order) --> */}
                    <div id="div-container-order" className="container bg-info p-2 jumbotron" style={{display:"none"}}>
                        <div id="div-order-infor" className="text-white p-3">...</div>
                        <div className="p-2">
                            <button type="button" className="btn btn-warning w-100">
                                Gửi đơn
                            </button>
                        </div>
                    </div>


                </div>
            </div>
        )
    }
}
export default form