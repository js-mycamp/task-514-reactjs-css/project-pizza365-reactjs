import Image1 from "../../../assets/Images/1.jpg";
import Image2 from "../../../assets/Images/2.jpg";
import Image3 from "../../../assets/Images/3.jpg";
import Image4 from "../../../assets/Images/4.jpg";
import Image5 from "../../../assets/Images/feature-saladbar.jpg";
import Carousel from 'react-bootstrap/Carousel';


const { Component } = require("react");




class CarouselComponent extends Component {
    render() {
        return (
            <div>
                <div className="row">

                    <div className="col-sm-12 text-warning">
                        <h1><b>Pizza 365</b></h1>
                        <p className="text-warning" style={{ fontStyle: "italic" }}>Truly italian!</p>
                    </div>


                    <div className="col-sm-12">
                        {/* thêm */}
                        <Carousel fade>
                            
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={Image1}
                                    alt="First slide"
                                />
                                <Carousel.Caption>

                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={Image2}
                                    alt="Second slide"
                                />

                                <Carousel.Caption>

                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={Image3}
                                    alt="Third slide"
                                />
                            </Carousel.Item>
                            
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={Image4}
                                    alt="Four slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100 "
                                    src={Image5 }
                                    alt="Five slide"
                                    style={{ width: '1320px', height: '880px' }}
                                />
                            </Carousel.Item>
                        </Carousel>
                        {/* thêm */}
                     
                    </div>
                </div>
            </div>
        )
    }
}
export default CarouselComponent