
const { Component } = require("react");


class whyPizza365 extends Component {
    render() {
        return(
            <div>
                 <div className="col-sm-12 text-center p-4 mt-4">
                        <h2><b className="p-2 border-bottom text-warning">Tại sao lại Pizza 365</b></h2>
                    </div>
                 <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-3 p-4  border" style={{backgroundColor:"lightgoldenrodyellow"}}>
                                <h3 className="p-2">Đa dạng</h3>
                                <p className="p-2">Số lượng pizza đa dạng. có đầy đủ các loại pizza đang hot nhất hiện nay.
                                </p>
                            </div>
                            <div className="col-sm-3 p-4  border" style={{backgroundColor:"yellow"}} >
                                <h3 className="p-2">Chất lượng</h3>
                                <p className="p-2">Nguyên liệu sạch 100% rõ nguồn góc,quy trình chết biến được đảm bảo vệ
                                    sinh an toàn thực phẩm
                                </p>
                            </div>
                            <div className="col-sm-3 p-4  border" style={{backgroundColor:"lightsalmon"}} >
                                <h3 className="p-2">Hương vị</h3>
                                <p className="p-2">Đảm bảo hương vị ngon, độc lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.
                                </p>
                            </div>
                            <div className="col-sm-3 p-4   border" style={{backgroundColor:"orange"}}>
                                <h3 className="p-2">Dịch Vụ</h3>
                                <p className="p-2">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất
                                    lượng. tân tiến</p>
                            </div>
                        </div>
                    </div>
            </div>
        )
    }
}
export default whyPizza365;