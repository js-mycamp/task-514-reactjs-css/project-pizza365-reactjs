import { Component } from "react";
import  CarouselComponent  from "./Carousel/carousel";
import WhyPizza365 from "./WhyPizza365/whyPizza365";
import Size from "./Size/Size"
import Drink from "./Drink/Drink";
import Type from "./Type/Type";
import Form from "./Form/Form";
class Content extends Component {
    render(){
        return(
            <>
             <CarouselComponent />
             
             <WhyPizza365 />
             <Size />
            <Type />

             <Drink />
             <Form />
            </>
           
        )
    }
}
export default Content