import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './component/Header/header';
import Content from './component/Content/Content';
import Footer from './component/Footer/Footer'
function App() {
  return (
    <div>
      {/* header */}
      <Header />
      {/* Content */}
      <div className="container" style={{padding: "120px 0 50px 0"}}>
        <Content />
      </div>
      <Footer />


    </div >
 )
}

export default App;
